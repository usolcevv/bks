
var slider = document.getElementById('slider'),
  input = document.getElementById('input-sum');

noUiSlider.create(slider, {
  start: 50000,
  connect: "lower",
  snap: true,
  step: 100000,
  range: {
    'min': 50000,
    'max': 2000000,
    '0%': 50000,
    '25%': 100000,
    '50%': 500000,
    '75%': 1000000,
    '100%': 2000000
  },

  format: wNumb({
    decimals: 0,
    thousand: ' ',
    postfix: '                 Р'
  }),

  pips: {
    mode: 'steps',
    values: [50000, 100000, 500000, 1000000, 2000000],
    density: 20,
    format: wNumb({
      decimals: 0,
      encoder: function(value) {
        return value / 1000;
      },
      postfix: ' т.'
    })
  }

});

slider.noUiSlider.on('update', function( values, handle ) {
  var val = parseInt(values[handle]);

  input.value = values[handle];
});

input.addEventListener('change', function(){
  var val = Number(values[handle]);
  slider.noUiSlider.set([null, values[handle]]);
});